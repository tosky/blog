<?php

Auth::routes();

// Dashboard
Route::get('dashboard', function (){
	return view('dashboard.index');
})->middleware('auth')->name('dashboard');

//  Dashboard posts
Route::resource('dashboard/posts', 'PostController');

// Dashboard user
Route::get('dashboard/user/{user}', 'UserController@show')->name('user.show');
Route::get('dashboard/user/{user}/edit', 'UserController@edit')->name('user.edit');
Route::put('dashboard/user/{user}', 'UserController@update')->name('user.update');

// Pages
Route::get('/', 'PageController@home')->name('home');
