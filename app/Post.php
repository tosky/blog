<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'title', 'body', 'image'
	];

    /**
     * Get the post's image path
     * @return string
     */
    public function getImageAttribute()
    {
        $filename = $this->attributes['image'];

        if (! $filename) {
            return 'images/post/no-image.jpg';
        }

        return 'storage/images/post/'.$filename;
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Store post image
     * @param  $request
     * @return string|null
     */
    public function storeImage($request)
    {
        if ($request->hasFile('image')) {
            $request->file('image')->store('images/post/', 'public');

            return $request->file('image')->hashName();
        }

        return null;
    }

    /**
     * Update post image
     * @param  $request
     * @return string|null
     */
    public function updateImage($request)
    {
        $filename = $this->attributes['image'];

        if ($request->hasFile('image')) {
            if ($filename) {
                $this->deleteImage();
                return $this->storeImage($request);
            }
            return $this->storeImage($request);
        }

        return $filename;
    }

    /**
     * Delete post image
     * @return bool
     */
    public function deleteImage()
    {
        $filename = $this->attributes['image'];

        if ($filename) {
            Storage::delete('public/images/post/'.$filename);

            return true;
        }

        return false;
    }
}
