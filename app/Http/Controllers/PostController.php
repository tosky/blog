<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Requests\BlogPostRequest;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all posts.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::where('user_id', auth()->id())->latest()->paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new post.
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a new post.
     *
     * @param  BlogPostRequest  $request
     * @param  Post $post
     * @return Response
     */
    public function store(BlogPostRequest $request, Post $post)
    {
        $filename = $post->storeImage($request);

        auth()->user()->publish(
            new Post([
                'title' => $request->title,
                'body' => $request->body,
                'image' => $filename
            ])
        );

        return redirect('dashboard/posts')->with(
            'message', 'Your post has now been published.'
        );
    }

    /**
     * Display the post.
     *
     * @param  Post   $post
     * @return Response
     */
    public function show(Post $post)
    {
        return view('posts.single', compact('post'));
    }

    /**
     * Show the form for editing the post.
     *
     * @param  Post  $post
     * @return Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the post.
     *
     * @param  BlogPostRequest  $request
     * @param  Post $post
     * @return Response
     */
    public function update(BlogPostRequest $request, Post $post)
    {
        $filename = $post->updateImage($request);

        $post->update([
            'title' => $request->title,
            'body' => $request->body,
            'image' => $filename
        ]);

        return redirect('dashboard/posts')->with(
            'message', 'Your post has been updated.'
        );
    }

    /**
     * Remove the post.
     *
     * @param  Post $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        $post->deleteImage();
        $post->delete();

        return redirect('dashboard/posts')->with(
            'message', 'Your post has been deleted.'
        );
    }
}
