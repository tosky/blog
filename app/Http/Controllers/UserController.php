<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display the user info
	 * @param  User   $user
	 * @return Response
	 */
    public function show(User $user)
    {
    	return view('user.profile', compact('user'));
    }

    /**
     * Show the form for editing user info
     * @param  User   $user
     * @return Response
     */
    public function edit(User $user)
    {
    	return view('user.edit', compact('user'));
    }

    /**
     * Update the user info
     * @param  UserRequest $request
     * @param  User        $user   
     * @return bool            
     */
    public function update(UserRequest $request, User $user)
    {
        $user->updateAvatar($request);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

    	return redirect()->route('user.show', ['id' => $user->id])->with(
    		'message', 'User profile has been updated.'
    	);
    }
}
