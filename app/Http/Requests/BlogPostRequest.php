<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class BlogPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->route('post') ? $request->route('post')->id : '';

        return [
            'title' => [
                'required',
                'max:255',
                'unique:posts,title,'.$id
            ],
            'body' => 'required',
            'image' => 'image'
        ];
    }
}
