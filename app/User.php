<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the user's avatar path
     * @return string
     */
    public function getAvatarAttribute()
    {
        if (! $this->attributes['avatar']) {
            return 'images/user/default-avatar.jpg';
        }

        return 'storage/images/user/avatar/'.$this->attributes['avatar'];
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Publish a new post
     * @param  Post   $post
     * @return Post
     */
    public function publish(Post $post)
    {
        return $this->posts()->save($post);
    }

    /**
     * Change user avatar
     * @param  $request
     * @return bool
     */
    public function updateAvatar($request)
    {
        $filename = $this->attributes['avatar'];

        if ($request->hasFile('avatar')) {
            if ($filename) {
                Storage::delete('public/images/user/avatar/'.$filename);
            }

            $request->file('avatar')->store('images/user/avatar/', 'public');

            $this->avatar = $request->file('avatar')->hashName();
            return $this->save();
        }

        return $filename;
    }
}
