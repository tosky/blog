<ul class="list-inline">
	<li class="list-inline-item">
		<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
		<span>{{ $post->created_at->diffForHumans() }}</span>
	</li>
	<li class="list-inline-item">
		<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
		<span><em>by</em> {{ $post->user->name }}</span>
	</li>
	<li class="list-inline-item">
		<span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
		<span>Category</span>
	</li>
</ul>