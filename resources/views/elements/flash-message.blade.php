@if ($flash = session('message'))
    <div id="flash" class="popover bottom flash-message">
    	<div class="arrow"></div>
    	<h3 class="popover-title alert-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Success</h3>
    	<div class="popover-content">
    		<p>{{ $flash }}</p>
    	</div>
    </div>
@endif