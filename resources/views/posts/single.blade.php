@extends('dashboard.master')

@section('body')
	<div class="row">
		<div class="col-md-12">
			<h1 class="post-title">{{ $post->title }}</h1>
			@include('elements.post-info')
		</div>
		<div class="col-md-4">
			<img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-responsive">
		</div>
		<div class="col-md-8">
			<p>{!! nl2br(e($post->body)) !!}</p>
		</div>
	</div>
@endsection