@extends('dashboard.master')

@section('body')
	<div class="table-responsive">
		<h2 class="page-header">All posts<a href="{{ route('posts.create') }}" class="btn btn-primary pull-right">
			<span class="glyphicon glyphicon-send"></span> Create new post
		</a></h2>
		<table class="table table-condensed">
			<thead>
				<th>#</th>
				<th>Title</th>
				<th>Image</th>
				<th>Body</th>
				<th>Created</th>
				<th>Updated</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($posts as $post)
				<tr>
					<td>{{ $post->id }}</td>
					<td>{{ $post->title }}</td>
					<td>
						<img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-circle" width="100" height="100">
					</td>
					<td>{{ str_limit($post->body, 255) }}</td>
					<td>{{ $post->created_at->format('jS F Y, H:i:s') }}</td>
					<td>{{ $post->updated_at->format('jS F Y, H:i:s') }}</td>
					<td width="250">
						<div class="btn-group">
							<a href="{{  route('posts.show', ['id' => $post->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a>

							<a href="{{  route('posts.edit', ['id' => $post->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>

							<a class="btn btn-danger" data-href="{{  route('posts.destroy', ['id' => $post->id]) }}" data-toggle="modal" data-target="#confirm-delete-{{ $post->id }}"><span class="glyphicon glyphicon-fire"></span> Delete</a>
						</div>

						<div class="modal fade" id="confirm-delete-{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header">
						                <b>Are youre sure to delete this post?</b>
						            </div>
						            <div class="modal-body">
					                	<span>Post: <a href="{{  route('posts.show', ['id' => $post->id]) }}"><b>{{ $post->title }}</b></a></span>
					            	</div>
						            <div class="modal-footer">
						                <form action="{{  route('posts.destroy', ['id' => $post->id]) }}" method="POST">
											{{ method_field('delete') }}
											{{ csrf_field() }}
											
						                	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						                	<button type="submit" class="btn btn-danger" data-href="{{  route('posts.destroy', ['id' => $post->id]) }}" data-toggle="modal" data-target="#confirm-delete">Delete</button>
										</form>
						            </div>
						        </div>
						    </div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection

@section('paginate')
	<div class="row">
		<div class="col-md-12">
			{{ $posts->links() }}
		</div>
	</div>
@endsection
