@extends('dashboard.master')

@section('body')
	<div class="row">
		<form action="{{ route('posts.update', ['id' => $post->id]) }}" method="POST" role="form" enctype="multipart/form-data"  class="col-md-12 form-horizontal" id="edit-post">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			
			<legend>Update post.</legend>

			<div class="col-md-8">
				<div class="form-group">
					<label for="post-title">Title:</label>
					<input type="text" id="post-title" class="form-control" name="title" value="{{ $post->title }}" required>
				</div>
				<div class="form-group">
					<label for="post-body">Body:</label>
					<textarea name="body" id="post-body" class="form-control" rows="5" required>{{ $post->body }}</textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button> 
				</div>
				<div class="form-group">
					@include('elements.errors')
				</div>
			</div>

			<div class="col-md-3 col-md-offset-1">
				<div class="col-md-12">
					<div class="form-group">
						<label for="image">Image:</label>
						<img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-responsive" id="image">
					    
					    <hr>
						<input type="file" name="image" onchange="loadFile(event)">
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection