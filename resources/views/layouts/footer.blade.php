<footer class="footer">
	<div class="container">
		<p class="copy text-center">Copyright &copy {{ Carbon\Carbon::now()->year }} {{ config('app.name')}} All Rights Reserved</p>
	</div>
</footer>