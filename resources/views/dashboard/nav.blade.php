<div class="dash-nav-wrapper">
    <div class="dash-nav-user">
        <img src="{{ asset(auth()->user()->avatar) }}" class="img-responsive">
        <p class="dash-nav-user-info">{{ auth()->user()->name }}</p>
    </div>
    <div class="dash-nav">
        <ul class="nav nav-pills nav-stacked navbar-default">
            <li>
                <a href="{{ route('dashboard') }}" class="{{ set_active('dashboard') }}">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('user.show', ['id' => auth()->id()]) }}" class="{{ set_active('dashboard/user/' . auth()->user()->id) }}">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> User
                </a>
          	</li>
            <li>
                <a href="{{ route('posts.index') }}" class="{{ set_active('dashboard/posts') }}">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Posts
                </a>
            </li>
        </ul>
    </div>
</div>