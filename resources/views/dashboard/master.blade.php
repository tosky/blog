@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.nav')
            </div>
            <div class="col-md-10">
                <div class="dash-body">
                    @yield('body')
                </div>
                    
                @yield('paginate')
            </div>

            @include('elements.flash-message')
        </div>
    </div>
@endsection
