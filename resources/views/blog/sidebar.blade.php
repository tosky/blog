@section('sidebar')
    <div class="panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title">Sidebar (ToDo)</h3>
        </div>
        <ul class="list-group">
        	<li class="list-group-item">Search</li>
        	<li class="list-group-item">About</li>
        	<li class="list-group-item">Popular posts</li>
        	<li class="list-group-item">Categories</li>
        	<li class="list-group-item">Newsletter</li>
        </ul>
    </div>
@show