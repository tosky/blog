@extends('dashboard.master')

@section('body')
	<div class="row">
		<form action="{{ route('user.update', ['id' => $user->id]) }}" method="POST" enctype="multipart/form-data" class="col-md-12 form-horizontal" role="form">
			{{ method_field('PUT') }}
			{{ csrf_field() }}

			<legend>Edit user profile</legend>
		
			<div class="col-md-3">
				<div class="form-group">
					<label for="post-thumbnail">Avatar:</label>
					<img src="{{ asset($user->avatar) }}" class="img-responsive thumbnail" id="image" width="350">
				    <hr>
					<input type="file" name="avatar" onchange="loadFile(event)">	
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<label for="username">Username:</label>
					<input type="text" name="name" class="form-control" id="username" value="{{ $user->name }}">
				</div>

				<div class="form-group">
					<label for="email">User email:</label>
					<input type="email" name="email" class="form-control" id="userEmail" value="{{ $user->email }}">
				</div>

				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" name="password" class="form-control" id="userEmail" value="{{ $user->password }}">
				</div>

				<div class="form-group">
					<label for="password_confirmation">Password confirm:</label>
					<input type="password" name="password_confirmation" class="form-control" id="userEmail" value="{{ $user->password }}">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
				
				<div class="form-group">
					@include('elements.errors')
				</div>
			</div>
		</form>
	</div>
@endsection