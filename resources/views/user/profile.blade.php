@extends('dashboard.master')

@section('body')
	<h2>User info</h2>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Avatar</th>
					<th>Username</th>
					<th>Email</th>
					<th>Posts</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
					<img src="{{ asset($user->avatar) }}" class="img-rounded" width="100" height="100">
					</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>Posts count</td>
				</tr>
			</tbody>
		</table>
	</div>
	<a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-primary">Edit profile</a>
@endsection